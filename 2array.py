import numpy as np
import sympy as sp
from symderiv import grad, hess, third, fourth
import dill
from functions import radial
from polylib import numderiv as nd

f = radial.r
X_test = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9], dtype=float)
#X_test = np.array([1, 2], dtype=float)

df = radial.dr

print("Imported a test function")

V = f(X_test)
G = df(X_test, 1)
H = df(X_test, 2)
T = df(X_test, 3)
#F = df(X_test, 4)

print("Analytic")
print(V)


print("Symbolic")

# Just for checking the supported input types: numpy array with ndim == 1 and just a symbol
input_mode = "array"

def r(x):
	"""
	The radial distance function
	"""
	res = (sum(s**2 for s in x))**(1/2)
	if input_mode == "array":
		res = np.array([res])
	return res

print("Grad test")
# You can compute a value at a single point
symb_grad = grad(X_test, r)
print(symb_grad.shape)
if input_mode == "array":
	ratio_grad = symb_grad[..., 0] / G
else:
	ratio_grad = symb_grad / G
print(ratio_grad)

exit()


print("Hess test")
# You can also get a function to later evaluate at any point
H2 = hess(X_test, r, lambdify=True)
if input_mode == "array":
	ratio_hess = H2(X_test)[..., 0] / H
else:
	ratio_hess = H2(X_test) / H
print(ratio_hess)


print("Third test")
symb_third = third(X_test, r, parallel=True)
print(symb_third.shape)
if input_mode == "array":
	ratio_third = symb_third[...,0] / T
else:
	ratio_third = symb_third / T
print(ratio_third[0, 0])

print("Fourth test")
#We precomputed this, now it can just be loaded from a file
if False: #In case you need to recompute
	fourth_func = fourth(X_test, r, lambdify=True, parallel=False)
	oname = "data/fourth_data.pkl"
	ofile = open(oname,'wb')
	dill.dump(fourth_func, ofile)

F_better = nd.fourth4fromhess(X_test, H2, h=1e-2)

with open("data/fourth_data.pkl", 'rb') as f:
	fourth_func = dill.load(f)
symb_fourth = fourth_func(X_test)
print(symb_fourth.shape)
if input_mode == "array":
	ratio_fourth = (symb_fourth[..., 0] / F_better[..., 0])
else:
	ratio_fourth = symb_fourth / F_better[..., 0]
print(f"Max ratio (should be close to 1): {np.max(ratio_fourth)}")
print(f"Min ratio (should be close to 1): {np.min(ratio_fourth)}")
print(ratio_fourth[0, 0])