import numpy as np
import sympy as sp
import symderiv as sd

x, y, z = sp.symbols('x y z')
expr = sp.sqrt(x**2 + y**2 + z**2) # This is a sympy expression

print("We have the following function")
print(expr)

df_dx = expr.diff(x)
df_dy = expr.diff(y)
df_dz = expr.diff(z)

#Convert it into a numpy function
df_dx_func = sp.lambdify((x, y, z), df_dx, 'numpy')

print("Symbolical derivative w. r. t. x")
print(df_dx)

print("Evaluated at a point")
point = np.array([1, 2, 3])
print(df_dx_func(*point))

print("Sympy also playes reasonably well with numpy.")
a = np.array([x, y, z])
print(a)

print("That allows us to do some vector operations.")
product = a.dot(a)
print(product)
cross = np.cross(a, a)
print(cross)

print("Vectors (and tensors) can also be element-wise differentiated using symderiv.")
l = sd.diff(a, x)
print(l)

print("We can also prepare a function with parameters.")
params = np.array([1, 2, 3])
params_symb = sd.assymbols(params, name="a")
print(params_symb)
params_vec = a**params_symb
print(params_vec)

print("We can still differentiate it:")
# We use our custom function to differentiate element-wise
df_dx = sd.diff(params_vec, x)
print(df_dx)
print(params_vec)
substitutions =  {symbol: value for symbol, value in zip(params_symb, params)}
concrete_params_vec = np.array([ele.subs(substitutions) for ele in params_vec])
df_dx_func = sd.array_lambdify((x, y, z), concrete_params_vec, modules="numpy")
df_dx_func = sd.array_to_func(df_dx_func)
print(df_dx_func(*point))