"""
Contains a function for computing chain rule and its derivatives.
Tests included.
"""
__version__ = "1.0.0"
__author__ = "Jindra Dušek"
from opt_einsum import contract




def chain_rule(f: list, g: list, n: int):
	"""
	Implements Faà di Bruno's formula, i. e.
	computes the nth derivative of the function
	f(g(x))
	See also: https://en.wikipedia.org/wiki/Fa%C3%A0_di_Bruno%27s_formula#Multivariate_version


	f, g: list of derivatives of the functions
	f[0] -> not important
	f[1] -> gradient
	f[2] -> hessian
	etc.
	"""
	assert len(f) > n
	assert len(g) > n

	if n == 1:
		res = contract("i, ai->a", f[1], g[1])
	elif n == 2:
		res = contract("ij, ai, bj->ab", f[2], g[1], g[1])
		res += contract("i, abi->ab", f[1], g[2])
	elif n == 3:
		res =  contract("ijk, ai, bj, ck->abc", f[3], g[1], g[1], g[1])
		res += contract("i, abci->abc", f[1], g[3])
		res += contract("ij, ai, bcj->abc", f[2], g[1], g[2])
		res += contract("ij, bi, acj->abc", f[2], g[1], g[2])
		res += contract("ij, ci, abj->abc", f[2], g[1], g[2])
	elif n == 4:
		res =  contract("ijkl, ai, bj, ck, dl->abcd", f[4], g[1], g[1], g[1], g[1])
		res += contract("i, abcdi->abcd", f[1], g[4])
		#4 terms
		res += contract("ij, ai, bcdj->abcd", f[2], g[1], g[3])
		res += contract("ij, bi, acdj->abcd", f[2], g[1], g[3])
		res += contract("ij, ci, abdj->abcd", f[2], g[1], g[3])
		res += contract("ij, di, abcj->abcd", f[2], g[1], g[3])
		#3 terms
		res += contract("ij, adi, bcj->abcd", f[2], g[2], g[2])
		res += contract("ij, bdi, acj->abcd", f[2], g[2], g[2])
		res += contract("ij, cdi, abj->abcd", f[2], g[2], g[2])
		#6 terms
		res += contract("ijk, ai, dk, bcj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, bi, dk, acj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, ci, dk, abj->abcd", f[3], g[1], g[1], g[2])
		res += contract("ijk, adi, bj, ck->abcd", f[3], g[2], g[1], g[1])
		res += contract("ijk, ai, bdj, ck->abcd", f[3], g[1], g[2], g[1])
		res += contract("ijk, ai, bj, cdk->abcd", f[3], g[1], g[1], g[2])
	else:
		raise Exception(f"n={n} not implemented.")
	return res

if __name__ == "__main__":
	import numpy as np
	from polylib import numderiv as nd

	#Testing chain rule on a typical case: a function evaluated in different coordinates

	def coord(X):
		"""
		The coordinate transform from cartesian coordinates into cyllindrical coordinates
		x, y, z -> r, phi, z
		"""
		x = X[0]
		y = X[1]
		z = X[2]
		r = np.sqrt(x**2 + y**2)
		phi = np.arctan2(y, x)
		res = np.zeros(3)

		res[0] = r
		res[1] = phi
		res[2] = z
		return res

	def inv_coord(X):
		"""
		The inverse coordinate transform
		r, phi, z -> x, y, z
		"""
		r   = X[0]
		phi = X[1]
		z   = X[2]
		res = np.zeros(3)
		res[0] = r*np.cos(phi)
		res[1] = r*np.sin(phi)
		res[2] = z
		return res

	if False:
		#Going back and forth to test things.
		x_cart = [23.15, 0.251, -6.31]
		x_cyll = coord(x_cart)
		x_cart2 = inv_coord(x_cyll)
		x_cyll2 = coord(x_cart2)
		print(x_cart / x_cart2)
		print(x_cyll / x_cyll2)

	def coord_der(X):
		"""
		The jacobian matrix d(r,phi,z)/d(x,y,z)
		"""
		res = np.zeros((3, 3))
		x = X[0]
		y = X[1]
		z = X[2]
		r = np.sqrt(x**2 + y**2)
		#phi = np.arctan2(y, x)

		#r
		res[0, 0] = x / r
		res[1, 0] = y / r
		res[2, 0] = 0

		#phi
		res[0, 1] = -y / r**2
		res[1, 1] =  x / r**2
		res[2, 1] =  0

		#z
		res[0, 2] = 0
		res[1, 2] = 0
		res[2, 2] = 1
		return res

	def coord_hess(X):
		res = np.zeros((3, 3, 3))
		x = X[0]
		y = X[1]
		z = X[2]
		r = np.sqrt(x**2 + y**2)
		#phi = np.arctan2(x, y)
		#A useful identity
		#d/dx 1/r = -x/r**(3)
		#https://www.wolframalpha.com/input?i=d+%281%2F%28x%5E2%2By%5E2%29%5E%281.%2F2.%29%29+%2F+dx+

		#r
		#x / r
		res[0, 0, 0] =  1/r - x**2/(r**3)
		res[0, 1, 0] = -y*x/(r**3)
		res[0, 2, 0] =  0

		#y / r
		res[1, 0, 0] = -y*x/(r**3)
		res[1, 1, 0] = 1/r - y**2/(r**3)
		res[1, 2, 0] = 0

		#0
		res[2, 0, 0] = 0
		res[2, 1, 0] = 0
		res[2, 2, 0] = 0

		##1/r**2 - y**2/r**4
		#phi
		#-y/r**2
		res[0, 0, 1] = 2*y*x/(r**4)
		res[0, 1, 1] = -1/r**2 + 2*y**2/r**4
		res[0, 2, 1] = 0

		#x/r**2
		res[1, 0, 1] = -1/r**2 + 2*y**2/r**4
		res[1, 1, 1] = -2*y*x/(r**4)
		res[1, 2, 1] =  0

		res[2, 0, 1] = 0
		res[2, 1, 1] = 0
		res[2, 2, 1] = 0

		#z: zeros

		return res


	def coord_third_der(X):
		#Would be too tedious to do analytically
		res = nd.grad4(X, coord_hess, h=1.e-3, fshape=(3, 3, 3))
		return res

	def coord_fourth_der(X):
		#Would be too tedious to do analytically
		res = nd.hess4(X, coord_hess, h=1.e-3)
		return res

	if False:
		pass
		#Test whether third and fourth der are converged

	x = np.array([23.15, 0.251, -6.31])
	if True:
		print("Testing coordinate transforms...")
		h = 1.e-3
		jac_num = nd.grad4(x, coord, h, fshape=(3,))
		jac_an = coord_der(x)
		assert jac_an.shape == jac_num.shape
		ratio_jac = jac_an / jac_num
		assert np.allclose(jac_an, jac_num)

		hess_num = nd.hess4(x, coord, h)
		hess_an = coord_hess(x)
		assert hess_an.shape == hess_num.shape
		ratio_hess = hess_an / hess_num
		assert np.allclose(hess_an, hess_num)

		print("Coordinate transform tests passed.")
		print()


	######
	##We define a function V_cart, and then compare it to V_cyll(coord(x_cart))
	a_pot = 1
	b_pot = 2
	c_pot = 3
	d_pot = 4

	def V_cyll(X_cyll):
		"""
		A test function, which represents some potential.
		Cyllindrical coordinates.
		"""
		rho, phi, z = X_cyll[0], X_cyll[1], X_cyll[2]
		res = -a_pot/(z**2 + rho**2)**(3./2.) + b_pot*np.exp(-c_pot*np.abs(z)) + d_pot*np.cos(phi)
		return res

	def G_cyll(X_cyll):
		"""
		The gradient of V_cyll. (Need analytically, because otherwise the fourth derivative won't be precise enough).
		"""
		rho, phi, z = X_cyll[0], X_cyll[1], X_cyll[2]
		res = np.zeros(3)
		K = (z**2 + rho**2)
		res[0] = 3*a_pot*rho/K**(5./2.)
		res[1] = -d_pot*np.sin(phi)
		res[2] = 3*a_pot*z/K**(5./2.) - c_pot* b_pot*np.exp(-c_pot*np.abs(z)) * np.sign(z)
		return res

	def V_cart(X_cart):
		"""
		A test function, which represents some potential.
		Cartesian coordinates.
		"""
		cyll = coord(X_cart)
		res = V_cyll(cyll)
		return res

	x_cart = [23.15, 0.251, -6.31]
	x_cyll = coord(x_cart)

	#Checking G_cyll
	G_cyll_an  = G_cyll(x_cyll)
	G_cyll_num = nd.grad4(x_cyll, V_cyll, h)
	assert np.allclose(G_cyll_an, G_cyll_num)

	print("Testing chain rule...")



	###Preparing variables
	#f(x)
	jac_cyll    = G_cyll(x_cyll)
	hess_cyll   = nd.grad4(x_cyll, G_cyll, h, fshape=(3,))
	third_cyll  = nd.hess4(x_cyll, G_cyll, h)
	fourth_cyll = nd.thirdtensor4(x_cyll, G_cyll, h*10)
	f_list_cyll = [0, jac_cyll, hess_cyll, third_cyll, fourth_cyll]

	#g(x)
	jac_coord    = coord_der(x_cart)
	hess_coord   = coord_hess(x_cart)
	third_coord  = coord_third_der(x_cart)
	fourth_coord = coord_fourth_der(x_cart)
	g_list = [0, jac_coord, hess_coord, third_coord, fourth_coord]

	#f(g(x))
	jac_cart    = nd.grad4(x_cart, V_cart, h)
	hess_cart   = nd.hess4(x_cart, V_cart, h)
	third_cart  = nd.thirdtensor4(x_cart, V_cart, h*10)
	fourth_cart = nd.fourthtensor4(x_cart, V_cart, h*100)
	f_list_cart = [0, jac_cart, hess_cart, third_cart, fourth_cart]


	if True:
		print("Subtest: testing that numerical derivatives are converged w. r. t. step size")
		h2 = h*10
		jac_cart2    = nd.grad4(x_cart, V_cart, h2)
		hess_cart2   = nd.hess4(x_cart, V_cart, h2)
		third_cart2  = nd.thirdtensor4(x_cart, V_cart, h2*10)
		fourth_cart2 = nd.fourthtensor4(x_cart, V_cart, h2*100)
		assert np.allclose(jac_cart, jac_cart2)
		assert np.allclose(hess_cart, hess_cart2)
		assert np.allclose(third_cart, third_cart2)
		assert np.allclose(fourth_cart, fourth_cart2, atol=1e-2) #Note: this one is not so precise

		jac_cyll2    = nd.grad4(x_cyll, V_cyll, h2)
		hess_cyll2   = nd.hess4(x_cyll, V_cyll, h2)
		third_cyll2  = nd.thirdtensor4(x_cyll, V_cyll, h2)
		fourth_cyll2 = nd.fourthtensor4(x_cyll, V_cyll, h2*10)
		assert np.allclose(jac_cyll    , jac_cyll2)
		assert np.allclose(hess_cyll   , hess_cyll2)
		assert np.allclose(third_cyll  , third_cyll2)
		assert np.allclose(fourth_cyll , fourth_cyll2)
		print("Subtest: Assertions passed.")

	jac_cart_chr    = chain_rule(f_list_cyll, g_list, n=1)
	hess_cart_chr   = chain_rule(f_list_cyll, g_list, n=2)
	third_cart_chr  = chain_rule(f_list_cyll, g_list, n=3)
	fourth_cart_chr = chain_rule(f_list_cyll, g_list, n=4)

	assert np.allclose(jac_cart_chr, jac_cart)
	assert np.allclose(hess_cart_chr,   hess_cart)
	assert np.allclose(third_cart_chr,  third_cart)
	assert np.allclose(fourth_cart_chr, fourth_cart)

	print("Assertions passed.")
	print("Test completed successfully.")