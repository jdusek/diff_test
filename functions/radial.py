from typing import Union
import numpy as np
from polylib import numderiv as nd
from functions.errors import RMSD

def r(x: np.ndarray) -> Union[np.ndarray, float]:
	"""
	The radial distance function
	"""
	return (np.sum(x**2, axis=-1))**(1./2)

def dr(x: np.ndarray, n: int) -> np.ndarray:
	"""
	Differentiates the radial distance function r.
	We are cheating a bit for higher derivatives.
	"""
	r0 = (np.sum(x**2, axis=-1))
	if n == 1:
		res = x / np.expand_dims(r0**(1./2.), axis=-1)
	elif n == 2:
		n = x.shape[-1]
		dot = np.sum(x**2, axis=-1)

		res = np.zeros(x.shape + (n,))
		for i in range(n):
			for j in range(i):
				res[..., i, j] = -x[i]*x[j]
		res += np.moveaxis(res, -1, -2)

		for i in range(n):
			res[..., i, i] = dot - x[..., i]**2

		res /= r0**(3/2)

	elif n == 3:
		print("Warning: doing the third derivative numerically")
		h = 1.e-3
		res = nd.thirdtensor4(x, r, h)

	elif n == 4:
		print("Warning: doing the fourth derivative numerically")
		h = 1.e-2
		res = nd.fourthtensor4(x, r, h)

	return res


if __name__ == "__main__":
	# Example usage:
	points = np.array([1, 2, 3])
	distances = r(points)

	derivatives = dr(points, 2)  # Computes second derivatives
	print("Radial distances:", distances)
	#print("Second derivatives with respect to x:", derivatives)


	print("Testing the derivatives")
	from polylib import numderiv as nd

	X_test = np.array([10, 20, -30])
	h = 1.e-3

	G_ana = dr(X_test, 1)
	H_ana = dr(X_test, 2)
	T_ana = dr(X_test, 3)
	F_ana = dr(X_test, 4)

	G_num = nd.grad4(X_test, r, h)
	H_num = nd.hess4(X_test, r, h)
	T_num = nd.thirdtensor4(X_test, r, h)
	F_num = nd.fourthtensor4(X_test, r, h)


	G_RMSD = RMSD(G_ana, G_num)
	H_RMSD = RMSD(H_ana, H_num)
	T_RMSD = RMSD(T_ana, T_num)
	#F_RMSD = RMSD(F_ana, F_num)

	print(f"G root mean square deviation: {G_RMSD:.3e} (should be {h**4:.1e})")
	print(f"H root mean square deviation: {H_RMSD:.3e} (should be {h**4:.1e})")
	#print(f"Root mean square deviation: {T_RMSD:.3e} (should be {h**4:.1e})")
	#print(f"Root mean square deviation: {F_RMSD:.3e} (should be {h**4:.1e})")

