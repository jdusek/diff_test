import numpy as np

def errors(approx, true, order=2, n0=0, n1=1):
	#relative = (approx - true) / true
	ratio = approx / true
	res = ratio
	if order==2:
		r1 = res[..., n1, n1]
		r0 = res[..., n0, n0]
	if order==3:
		r1 = res[..., n1, n1, n1]
		r0 = res[..., n0, n0, n0]
	elif order==4:
		r1 = res[..., n1, n1, n1, n1]
		r0 = res[..., n0, n0, n0, n0]
	return r0, r1


def RMSD(true: np.ndarray, approx: np.ndarray) -> float:
	res = np.sqrt(np.sum(np.square(true - approx)) / true.size)
	return res

def max_err(true: np.ndarray, approx: np.ndarray) -> float:
	"""
	Returns the maximum *relative* error.
	"""
	return np.max(np.abs((true-approx)/true))

def pos_max_err(true: np.ndarray, approx: np.ndarray) -> tuple:
	"""
	Returns the position of the maximum relative error.
	"""
	res = np.unravel_index((np.abs((true-approx)/true)).argmax(),approx.shape)
	return res