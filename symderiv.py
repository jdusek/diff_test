"""
# Symderiv

Calculate derivatives symbolically with sympy. Currently, there is grad, hess, third and fourth.

E. g.:
hess_func = hess(x, f, lambdify=True, parallel=True)

where x is a numpy array of floats and f is a function.
With this, hess_func will be a callable function that
takes in numpy arrays of floats and returns the hessian matrix.

Under the hood, the derivative will be calculated using symbolical
operations.

## Limitations
1. The function f has to be able to manipulate sympy expressions.
This creates a slight difficulty when you have e. g.
f(x) = cos(x[0]) * x[1]
2. The input and output of f has to be either
  - a numpy array (of sympy symbols) with ndim == 1 (i. e. a vector), or
  - a sympy symbol (so not a numpy array).
3. The symbolic differentiation can be slow.

How to get around them:
1. Use symnummath, which provides functions that work with numpy and numpy.
2. Flatten your input and output arrays and then reshape them later as needed.
3. You can enable parallelisation.
"""
__author__ = 'Jindra Dušek'


from typing import TypeVar, Union, Callable, List
from functools import partial
#from multiprocessing import Pool
from pathos.multiprocessing import Pool
import numpy as np
import sympy as sp
import math
import logging


# Setting up logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.propagate = False

T = TypeVar('T') # Any possible type
num_processes=6
#Expand this when you get an error like NameError: name 'acos' is not defined
symderiv_modules = ["numpy", "scipy", { "acos": np.arccos, "asin": np.arcsin}, "sympy"]

logger.debug(f"Maximum number of processes: {num_processes}")

################
# How the derivative functions roughly work:
# 1. Generate a sympy array in the shape of the numpy array input.
# 2. Generate the initial expression from f.
# 3. Differentiate it to obtain the derivative as a sympy expression.
# 4. Create a lambda function from it using sympy.lambdify.
# 5. Return either the function or its value at the input.


def assymbols(x: np.ndarray, name: str="x") -> np.ndarray[object]: #List of sympy symbols
	"""Create a numpy array of symbols in the shape of the input numpy array."""
	n = len(x)
	sympy_array = np.asarray([ sp.symbols(f'{name}_{i}') for i in range(n)])
	return sympy_array

def generate_symbols(x: np.ndarray, f):
	x = assymbols(x)
	res = f(x)
	return res

def array_lambdify(x, expr_arr: np.ndarray, modules, parallel=True):
	if not parallel:
		res = np.empty(expr_arr.shape, dtype=object)
		for index, element in np.ndenumerate(expr_arr):
			res[index] = sp.lambdify(x, element, modules=modules)
	else:
		logger.debug("START: Starting lambdify (usually takes the longest time)")
		def my_lambdify(x, expr_arr, index, modules):
			logger.debug(f"{index} out of {expr_arr.shape}")
			return index, sp.lambdify(x, expr_arr[index], modules=modules)

		res = np.empty(expr_arr.shape, dtype=object)

		iterable = [ (x, expr_arr, index, modules) for index, val in np.ndenumerate(expr_arr)]

		with Pool(processes=num_processes) as pool:
			results = pool.starmap(my_lambdify, iterable)

		for index, value in results:
			res[index] = value
	return res

def array_evaluate(func_arr: np.ndarray, *args):
	"""
	Given an array of functions, return an array of their evaluations.
	e. g. [f, g] -> [f(x), g(x)]
	"""
	res = np.empty(func_arr.shape, dtype=float)
	for index, func in np.ndenumerate(func_arr):
		res[index] = func(*args)
	return res

def array_to_func(func_arr):
	"""Given an array of functions, return a function that returns an array."""
	def func(*args):
		res = array_evaluate(func_arr, *args)
		return res
	return func

def diff(expr: Union[np.ndarray, sp.Symbol], x: sp.Symbol):
	if isinstance(expr, np.ndarray):
		res = np.empty(expr.shape, dtype=object)
		for index, element in np.ndenumerate(expr):
			res[index] = element.diff(x)
	else:
		res = expr.diff(x)
	return res


def calculate_diff(x, expr, i):
	derivative = diff(expr, x[i])
	res = (i, derivative)
	return res

def no_elements(dim: int, rank: int, sym: bool=True):
	"""
	https://www.physics.mcgill.ca/~yangob/symmetric%20tensor.pdf
	"""
	if sym:
		res = math.comb(dim+rank-1,rank)
	else:
		res = dim**rank
	return res

def symbolic_grad(x, expr):
	"""
	Given symbols x and an expression expr, calculate the symbolic gradient.
	"""
	#Unparallelised version
	#res = [ diff(expr, x1) for x1 in x]
	#res = np.asarray(res)
	#return res
	logger.debug("START: Starting symbolic grad")

	n = len(x)
	m = 0
	if isinstance(expr, np.ndarray):
		m = expr.size
	shape_res = (n,) + (m,) if m > 0 else (n,)
	res = np.empty(shape_res, dtype=object)
	partial_diff = partial(calculate_diff, x, expr)
	with Pool(processes=num_processes) as pool:
		results = pool.map(partial_diff, range(n))

	for i, result in results:
		res[i] = result
	res = np.asarray(res)
	return res

def symbolic_hess(x, expr):
	"""
	Given symbols x and an expression expr, calculate the symbolic hessian.
	"""
	gr = symbolic_grad(x, expr)
	logger.debug("START: Starting symbolic hess")

	##Unparallelised version
	#res = [ diff(gr, x1) for x1 in x]
	#res = np.asarray(res)
	#return res

	n = len(x)
	m = 0
	if isinstance(expr, np.ndarray):
		m = expr.size
	shape_res = (n, n) + (m,) if m > 0 else (n, n)
	shape_ele = (n,) + (m,) if m > 0 else (n,)
	res = np.empty(shape_res, dtype=object)
	for i in range(n):
		element = np.empty(shape_ele, dtype=object)
		partial_diff = partial(calculate_diff, x, gr[i])
		with Pool(processes=num_processes) as pool:
			results = pool.map(partial_diff, range(n))
		for a, result in results:
			element[a] = result
		res[i] = element
	res = np.asarray(res)
	return res

def symbolic_third(x, expr):
	"""
	Given symbols x and an expression expr, calculate the symbolic third derivatives.
	"""
	hess = symbolic_hess(x, expr)
	logger.debug("START: Starting symbolic third")

	##Unparallelised version
	#res = [ diff(hess, x1) for x1 in x ]
	#res = np.asarray(res)
	#return res

	n = len(x)
	m = 0
	if isinstance(expr, np.ndarray):
		m = expr.size
	shape_res = (n, n, n) + (m,) if m > 0 else (n, n, n)
	shape_ele = (n,) + (m,) if m > 0 else (n,)
	res = np.empty(shape_res, dtype=object)
	for i in range(n):
		for j in range(n):
			element = np.empty(shape_ele, dtype=object)
			partial_diff = partial(calculate_diff, x, hess[i][j])
			with Pool(processes=num_processes) as pool:
				results = pool.map(partial_diff, range(n))
			for k, result in results:
				element[k] = result
			res[i][j] = element
	res = np.asarray(res)
	return res

def symbolic_fourth(x, expr):
	"""
	Given symbols x and an expression expr, calculate the symbolic fourth derivatives.
	"""
	third = symbolic_third(x, expr)
	logger.debug("START: Starting symbolic fourth")

	##Unparallelised version
	#res = [ diff(third, x1) for x1 in x ]
	#res = np.asarray(res)
	#return res

	n = len(x)
	m = 0
	if isinstance(expr, np.ndarray):
		m = expr.size
	shape_res = (n, n, n, n) + (m,) if m > 0 else (n, n, n, n)
	shape_ele = (n,) + (m,) if m > 0 else (n,)
	res = np.empty(shape_res, dtype=object)
	for i in range(n):
		for j in range(n):
			for k in range(n):
				element = np.empty(shape_ele, dtype=object)
				partial_diff = partial(calculate_diff, x, third[i][j][k])
				with Pool(processes=num_processes) as pool:
					results = pool.map(partial_diff, range(n))
				for l, result in results:
					element[l] = result
				res[i][j][k] = element
	return res


def grad(x: np.ndarray, f, lambdify: bool=False, parallel: bool=True) -> Union[Callable[[np.ndarray], np.ndarray], np.ndarray]:
	expr = generate_symbols(x, f)
	x_symb = assymbols(x)
	res_symb = symbolic_grad(x_symb, expr)
	res = array_lambdify(x_symb, res_symb, 'numpy', parallel=parallel)
	res = array_to_func(res)
	def res_fun(xx):
		return res(*xx)
	if lambdify:
		return res_fun
	else:
		return res_fun(x)

def hess(x: np.ndarray, f, lambdify: bool=False, parallel: bool=True) -> Union[Callable[[np.ndarray], np.ndarray], np.ndarray]:
	expr = generate_symbols(x, f)
	x_symb = assymbols(x)
	res_symb = symbolic_hess(x_symb, expr)
	res = array_lambdify(x_symb, res_symb, 'numpy', parallel=parallel)
	res = array_to_func(res)
	def res_fun(xx):
		return res(*xx)
	if lambdify:
		return res_fun
	else:
		return res_fun(x)

def third(x: np.ndarray, f, lambdify: bool=False, parallel: bool=True) -> Union[Callable[[np.ndarray], np.ndarray], np.ndarray]:
	expr = generate_symbols(x, f)
	x_symb = assymbols(x)
	res_symb = symbolic_third(x_symb, expr)
	res = array_lambdify(x_symb, res_symb, 'numpy', parallel=parallel)
	res = array_to_func(res)
	def res_fun(xx):
		return res(*xx)
	if lambdify:
		return res_fun
	else:
		return res_fun(x)

def fourth(x: np.ndarray, f, lambdify: bool=False, parallel: bool=True) -> Union[Callable[[np.ndarray], np.ndarray], np.ndarray]:
	expr = generate_symbols(x, f)
	x_symb = assymbols(x)
	res_symb = symbolic_fourth(x_symb, expr)
	#print("symb done")
	res = array_lambdify(x_symb, res_symb, 'numpy', parallel=parallel)
	res = array_to_func(res)
	def res_fun(xx):
		return res(*xx)
	if lambdify:
		return res_fun
	else:
		return res_fun(x)
