import numpy as np
from polylib import numderiv as nd
import symnummath as smn
from symnummath import chain_rule

from functions import radial


one = np.array([1, 2, 3], dtype=float)
two = np.array([4, 5, 6], dtype=float)
X_test = np.concatenate((one, two))


r = radial.r
dr = radial.dr

def inner(x: np.ndarray, sympy_mode=None):
    """2->1"""
    r1 = r(x[0:3])
    r2 = r(x[3:6])
    res = np.array([r1, r2])
    return res

def outer(x: np.ndarray, sympy_mode=None):
    """6->2"""
    res = smn.cos(x[0], sympy_mode=sympy_mode)
    res *= smn.sin(x[1], sympy_mode=sympy_mode)
    return res

def func(x: np.ndarray, sympy_mode=None):
    """6->1"""
    y = inner(x)
    res = outer(y, sympy_mode=sympy_mode)
    return res

V = func(X_test)
Y_test = inner(X_test)


print("We will compute the derivatives of f using chain rule.")

outer_list = [0]
inner_list = [0]

folder = "./data/"
generate_all = False


print("##########################################")
print("Gradient")
# No chain rule derivative
n = 1

symb_grad = smn.memoized_der(X_test, lambda x: func(x, sympy_mode=True), n, folder + "full"+str(n)+".pkl", sympy_mode=generate_all)

inner1 = smn.memoized_der(X_test, lambda x: inner(x, sympy_mode=True), n, folder + "inner"+str(n)+".pkl", sympy_mode=generate_all)
outer1 = smn.memoized_der(Y_test, lambda x: outer(x, sympy_mode=True), n, folder + "outer"+str(n)+".pkl", sympy_mode=generate_all)
outer_list.append(outer1)
inner_list.append(inner1)


ch_grad = chain_rule(outer_list, inner_list, n)
print(symb_grad / ch_grad)


print("##########################################")
print("Hessian")
n = 2

symb_hess = smn.memoized_der(X_test, lambda x: func(x, sympy_mode=True), n, folder + "full"+str(n)+".pkl", sympy_mode=generate_all)

inner2 = smn.memoized_der(X_test, lambda x: inner(x, sympy_mode=True), n, folder + "inner"+str(n)+".pkl", sympy_mode=generate_all)
outer2 = smn.memoized_der(Y_test, lambda x: outer(x, sympy_mode=True), n, folder + "outer"+str(n)+".pkl", sympy_mode=generate_all)
outer_list.append(outer2)
inner_list.append(inner2)

ch_hess = chain_rule(outer_list, inner_list, n)
print(symb_hess / ch_hess)


print("##########################################")
print("Third")
n = 3

symb_third = smn.memoized_der(X_test, lambda x: func(x, sympy_mode=True), n, folder + "full"+str(n)+".pkl", sympy_mode=generate_all)
#T = nd.thirdtensor4(X_test, lambda x: func(x), h=1.e-3)
#ratio = symb_third / T
#print(f"Max ratio (should be close to 1): {np.nanmax(ratio)}")
#print(f"Min ratio (should be close to 1): {np.nanmin(ratio)}")
#print(ratio[0])

inner3 = smn.memoized_der(X_test, lambda x: inner(x, sympy_mode=True), n, folder + "inner"+str(n)+".pkl", sympy_mode=generate_all)
#T = nd.thirdtensor4(X_test, inner, h=1.e-3)
#ratio = inner3 /  np.where(T == 0, np.nan, T)
#print(f"Max ratio (should be close to 1): {np.nanmax(ratio)}")
#print(f"Min ratio (should be close to 1): {np.nanmin(ratio)}")
#print(ratio[0])
#exit()

outer3 = smn.memoized_der(Y_test, lambda x: outer(x, sympy_mode=True), n, folder + "outer"+str(n)+".pkl", sympy_mode=generate_all)
#T = nd.thirdtensor4(Y_test, outer, h=1.e-3)
#ratio = outer3 /  np.where(T == 0, np.nan, T)
#print(f"Max ratio (should be close to 1): {np.nanmax(ratio)}")
#print(f"Min ratio (should be close to 1): {np.nanmin(ratio)}")
#print(ratio)
outer_list.append(outer3)
inner_list.append(inner3)

ch_third = chain_rule(outer_list, inner_list, n)
ratio =  ch_third / symb_third
print(f"Max ratio (should be close to 1): {np.nanmax(ratio)}")
print(f"Min ratio (should be close to 1): {np.nanmin(ratio)}")
print(ratio[0])


print("##########################################")
print("Fourth")
n = 4
symb_fourth = smn.memoized_der(X_test, lambda x: func(x, sympy_mode=True), n, folder + "full"+str(n)+".pkl", sympy_mode=generate_all)

inner4 = smn.memoized_der(X_test, lambda x: inner(x, sympy_mode=True), n, folder + "inner"+str(n)+".pkl", sympy_mode=generate_all)
outer4 = smn.memoized_der(Y_test, lambda x: outer(x, sympy_mode=True), n, folder + "outer"+str(n)+".pkl", sympy_mode=generate_all)
outer_list.append(outer4)
inner_list.append(inner4)

ch_fourth = chain_rule(outer_list, inner_list, n)
ratio = symb_fourth / ch_fourth
print(f"Max ratio (should be close to 1): {np.nanmax(ratio)}")
print(f"Min ratio (should be close to 1): {np.nanmin(ratio)}")
print(ratio[0, 0])
