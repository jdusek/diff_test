# Diff test

A showcase of symbolic differentiation using [sympy](https://www.sympy.org/en/index.html).

Installation:

```
conda install sympy dill pathos
```

- You need need dill -- a better version of pickle -- for saving functions into a file.
- [pathos](https://pypi.org/project/pathos/) is a python library for multiprocessing. It is better than the default multiprocessing library, because it uses dill. We can thus share a function between various processes, which is essential in this library.


## Sympy and symbolic derivatives

